(ns plf01.core)

(defn función-<-1
  [e1 e2]
  (if (> e1 e2) 
    "elemento2 menor que elemento2" 
    "elemento1 menor que elemento1"))

(defn funcion-<-2
  [e1 e2 e3 e4]
  (cond
    (< e1 e2) true
    (< e3 e4) false
    :else "ni true ni false"
    ))

(defn funcion-<-3
  [num1]
  (if (< num1 (rand-int 11)) "perdiste" "ganaste"
   ))

(función-<-1 8 4)
(funcion-<-2 2 4 6 4)
(funcion-<-3 9)

(defn funcion-<=-1
  [num1]
  (if (<= num1 0)
    "negativo o 0"
    "positivo"
    ))

(defn funcion-<=-2
  [e1 e2]
  (cond (<= e1 e2) 1
        (<= e2 e1) 2
        ))

(defn funcion-<=-3
  [e1 e2 e3]
  (if (<= (+ e1 e2 e3) (+ 5 4 3))
    "pierdes"
    "ganas"
    ))

(funcion-<=-1 -1)
(funcion-<=-2 4 4)
(funcion-<=-3 1 2 11)

(defn funcion-==-1
  [el1 el2]
  (if (== el1 el2)
    "iguales"
    "diferentes"
    ))

(defn funcion-==-2 
  [b1 b2 b3]
  (
   let [xs [b1 b2 b3]]
   (if (== (count xs) (first xs))
     true
     false)
   )) 

(defn funcion-==-3 
  [n1 n2 n3 n4]
  (
   if(== (+ n1 n2 n3 n4) (* -1 (- n4 n3 n2 n1 n1 n1)))
   "si" 
   "no"
   ))

(funcion-==-1 3 3)
(funcion-==-2 3 2 2)
(funcion-==-3 1 1 1 1)

 (defn funcion->-1
   [el1 el2]
   (if (> el1 el2)
     "el1 mayor"
     "el2 mayor"
  ))

(defn funcion->-2
  [el1 el2 el3 el4]
  (if(> (+ el1 el2 el3 el4) 30)
    "ganas"
    "pierdes"
  ))

(defn funcion->-3
  [el1 el2 el3 el4 el5 el6]
  (
   if ( > (+ el1 el2 el3) (+ el4 el5 el6))
   "primer trio ganador"
   "segundo trio ganador"
   ))
 
(funcion->-1 1 0)
(funcion->-2 10 10 0 0)
(funcion->-3 11 2 3 4 5 6)

(defn funcion->=-1
  [num1 num2]
  (if (>= num1 num2)
    "num1 mayor o igual"
    "num1 menor"))

(defn funcion->=-2
  [el1]
  (cond (>= el1 (rand-int 10)) 1
        (>= (rand-int 10) (+ el1 el1)) 2
        ))

(defn funcion->=-3
  [e1 e2 e3]
  (if (>= (+ e1 e2 e3) (+ 5 4 3))
    "ganaste"
    "perdiste"))

(funcion->=-1 2 22)
(funcion->=-2 10)
(funcion->=-3 1 1 2)

(defn funcion-assoc-1
  [num1 num2 num3]
  (assoc {:a num1 :b num2}
         :a num3 :c num3)
  )

(defn funcion-assoc-2
  [a b]
  (assoc {:a a}
         :b b)
  )

(defn funcion-assoc-3
  [a]
  (assoc [true true 3] 2 a)  
         ))

(funcion-assoc-1 4 8 12)
(funcion-assoc-2 true false)
(funcion-assoc-3 true)




